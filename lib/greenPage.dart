import 'package:flutter/material.dart';


class GreenPage extends StatelessWidget {

  static const String routeName = '/green-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Green page'),
      ),
      body: Center(
        child: Container(
          color: Colors.green,
          child: Text(routeName,
          style: TextStyle(
            fontSize: 40
          ),),
        ),
      ),
    );
  }
}
