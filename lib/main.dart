import 'package:buttons_and_routes/greenPage.dart';
import 'package:buttons_and_routes/redPage.dart';
import 'package:flutter/material.dart';

import 'bluePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
      routes: {
        GreenPage.routeName: (ctx) => GreenPage(),
        RedPage.routeName: (ctx) => RedPage(),
        BluePage.routeName: (ctx) => BluePage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void changePage(BuildContext context, path) {
    Navigator.of(context).pushNamed(path);
  }

  Widget DrawerItem(BuildContext context, String path, String pageName, Color color) {
    return Container(
      height: 50,
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: color,
              blurRadius: 15,
              spreadRadius: 10,
              offset: Offset(5, 5)),
        ],
      ),
      margin: EdgeInsets.all(15),
      child: Material(
        color: Colors.white38,
        child: InkWell(
          onTap: () => changePage(context, path),
          splashColor: Colors.white,
          child: Center(
            child: Text(
              '$pageName page',
              style: TextStyle(fontSize: 30),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buttons And Routes'),
      ),
      drawer: Drawer(
        child: Padding(
          padding: EdgeInsets.only(top: 30),
          child: Column(
            children: [
              DrawerItem(context, GreenPage.routeName, 'Green', Colors.green),
              DrawerItem(context, RedPage.routeName, 'Red', Colors.red),
              DrawerItem(context, BluePage.routeName, 'Blue', Colors.blue),
            ],
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15),
        child: Icon(Icons.arrow_upward),
      ),
    );

  }
}
