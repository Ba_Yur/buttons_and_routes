import 'package:flutter/material.dart';


class RedPage extends StatelessWidget {

  static const String routeName = '/red-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Green page'),
      ),
      body: Center(
        child: Container(
          color: Colors.red,
          child: Text(routeName,
            style: TextStyle(
                fontSize: 40
            ),),
        ),
      ),
    );
  }
}
