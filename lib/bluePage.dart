import 'package:flutter/material.dart';


class BluePage extends StatelessWidget {

  static const String routeName = '/blue-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Blue page'),
      ),
      body: Center(
        child: Container(
          color: Colors.blue,
          child: Text(routeName,
            style: TextStyle(
                fontSize: 40
            ),),
        ),
      ),
    );
  }
}
